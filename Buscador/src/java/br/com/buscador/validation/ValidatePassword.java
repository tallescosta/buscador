package br.com.buscador.validation;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.User;

public class ValidatePassword implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        User user = (User) entity;
                
        if(user.getPassword().length() < 6){
            msg += "Senha inválida! (deve contar no mínimo 6 dígitos)\n";
        }
        
        return msg;
    }
    
}
