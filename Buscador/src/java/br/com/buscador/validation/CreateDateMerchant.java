package br.com.buscador.validation;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.Merchant;
import java.util.Date;

public class CreateDateMerchant implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        Merchant merchant = (Merchant) entity;
        Date now = new Date();
        
        merchant.setCreateDate(now);
        merchant.setUpdateDate(now);
        
        IStrategy createDateUser = new CreateDateUser();
        msg += createDateUser.process(merchant.getUser());
        
        return msg;
    }
    
}
