package br.com.buscador.validation;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.Merchant;

public class ValidateAtributesMerchant implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        Merchant merchant = (Merchant) entity;
        
        if(merchant.getCpf() == null || merchant.getCpf().trim().isEmpty()){
            msg += "CPF é um campo obrigatório!\n";
        }if(merchant.getName() == null || merchant.getName().trim().isEmpty()){
            msg += "Nome é um campo obrigatório!\n";
        }if(merchant.getBirthDate() == null || merchant.getBirthDate().toString().trim().isEmpty()){
            msg += "Data de Nasc. é um campo obrigatório!\n";
        }if(merchant.getGender() == null || merchant.getGender().trim().isEmpty()){
            msg += "Sexo é um campo obrigatório!\n";
        }
        
        IStrategy validateUser = new ValidateAtributesUser();
        msg += validateUser.process(merchant.getUser());
        
        IStrategy validateCpf = new ValidateCPF();
        msg += validateCpf.process(merchant);
        
        return msg;
    }
    
}
