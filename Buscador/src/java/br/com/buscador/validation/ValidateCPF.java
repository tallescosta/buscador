package br.com.buscador.validation;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.Merchant;

public class ValidateCPF implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        Merchant merchant = (Merchant) entity;
        
        if(merchant.getCpf().length() != 11){
            msg += "CPF inválido! (use somente números)\n";
        }
        
        return msg;
    }
    
}
