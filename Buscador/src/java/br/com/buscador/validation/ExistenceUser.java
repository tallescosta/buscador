package br.com.buscador.validation;

import br.com.buscador.persistence.UserDao;
import br.com.buscador.domain.Entity;
import br.com.buscador.domain.User;

public class ExistenceUser implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        User user = (User) entity;
        UserDao daoUser = new UserDao();
        
        if(daoUser.getByEmail(user.getEmail()).getId() != null){
            msg += "Email já cadastrado!\n";
        }
        
        return msg;
    }
    
}
