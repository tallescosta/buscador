package br.com.buscador.validation;

import br.com.buscador.domain.Entity;

public interface IStrategy {
    
    public String process(Entity entity);
    
}
