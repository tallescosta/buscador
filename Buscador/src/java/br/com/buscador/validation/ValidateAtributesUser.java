package br.com.buscador.validation;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.User;

public class ValidateAtributesUser implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        User user = (User) entity;
        
        if(user.getEmail() == null || user.getEmail().trim().isEmpty()){
            msg += "Email é um campo obrigatório!\n";
        } if(user.getPassword() == null || user.getPassword().trim().isEmpty()){
            msg += "Senha é um campo obrigatório!\n";
        }
        
        IStrategy validatePassword = new ValidatePassword();
        msg += validatePassword.process(user);
        
        return msg;
    }
    
}
