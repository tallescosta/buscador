package br.com.buscador.validation;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.User;
import java.util.Date;

public class CreateDateUser implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        User user = (User) entity;
        Date now = new Date();
        
        user.setCreateDate(now);
        user.setUpdateDate(now);
        
        return msg;
    }
    
}
