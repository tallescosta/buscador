package br.com.buscador.validation;

import br.com.buscador.persistence.MerchantDao;
import br.com.buscador.domain.Entity;
import br.com.buscador.domain.Merchant;

public class ExistenceMerchant implements IStrategy {

    @Override
    public String process(Entity entity) {
        String msg = "";
        Merchant merchant = (Merchant) entity;
        MerchantDao daoMerchant = new MerchantDao();
        
        if(daoMerchant.getByCpf(merchant.getCpf()).getId() != null){
            msg += "CPF já cadastrado!\n";
        }
        
        IStrategy existenceUser = new ExistenceUser();
        msg += existenceUser.process(merchant.getUser());
        
        return msg;
    }
    
}
