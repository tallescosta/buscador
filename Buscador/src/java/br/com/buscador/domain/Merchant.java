package br.com.buscador.domain;

import java.util.Date;

public class Merchant extends Person {
    
    private String cpf;
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public Merchant(User user) {
        super.setUser(user);
    }
    
    public Merchant(Long id, User user) {
        super.setId(id);
        super.setUser(user);
    }
    
    public Merchant(String cpf, String name, String gender, User user) {
        this.cpf = cpf;
        super.setName(name);
        super.setGender(gender);
        super.setUser(user);
    }
    
    public Merchant(String cpf, String name, String gender, Date birthDate, User user) {
        this.cpf = cpf;
        super.setName(name);
        super.setGender(gender);
        super.setBirthDate(birthDate);
        super.setUser(user);
    }
    
    public Merchant(Long id, String cpf, String name, String gender, User user) {
        super.setId(id);
        this.cpf = cpf;
        super.setName(name);
        super.setGender(gender);
        super.setUser(user);
    }
    
    public Merchant(Long id,String cpf, String name, String gender, Date birthDate, User user) {
        super.setId(id);
        this.cpf = cpf;
        super.setName(name);
        super.setGender(gender);
        super.setBirthDate(birthDate);
        super.setUser(user);
    }
    
}
