package br.com.buscador.domain;

public class User extends Entity{

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
    
    public User(Long id) {
        super.setId(id);
    }

    public User(Long id, String email, String password) {
        super.setId(id);
        this.email = email;
        this.password = password;
    }
    
}
