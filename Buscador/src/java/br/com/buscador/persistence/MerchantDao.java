package br.com.buscador.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.Merchant;
import br.com.buscador.domain.User;
import java.util.ArrayList;

public class MerchantDao implements IDao {
    
    private Connection conn;
    
    public MerchantDao() {
        conn = new ConnectionFactory().getConnection();
    }
    
    @Override
    public boolean save(Entity entity) {
        Merchant merchant = (Merchant) entity;
        
        // Persists the User
        UserDao userDao = new UserDao();
        if(!userDao.save(merchant.getUser())){
            return false;
        }
        
        merchant.setUser(userDao.getByEmail(merchant.getUser().getEmail()));
        
        String sql = "INSERT INTO buscador.merchant(createDate, updateDate, cpf, name, gender, birthDate, user_id) "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            statement.setTimestamp(1, new java.sql.Timestamp(merchant.getCreateDate().getTime()));
            statement.setTimestamp(2, new java.sql.Timestamp(merchant.getUpdateDate().getTime()));
            statement.setString(3, merchant.getCpf());
            statement.setString(4, merchant.getName());
            statement.setString(5, merchant.getGender());
            statement.setTimestamp(6, new java.sql.Timestamp(merchant.getBirthDate().getTime()));
            statement.setLong(7, merchant.getUser().getId());
            
            statement.execute();
            statement.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Entity entity) {
        Merchant merchant = (Merchant) entity;
        
        // Updates the User
        UserDao userDao = new UserDao();
        if(!userDao.update(merchant.getUser())){
            return false;
        }
        
        String sql = "UPDATE buscador.merchant "
                + "SET updateDate=?, cpf=?, name=?, gender=?, birthDate=? "
                + "WHERE id=?";
        
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            statement.setTimestamp(1, new java.sql.Timestamp(merchant.getUpdateDate().getTime()));
            statement.setString(2, merchant.getCpf());
            statement.setString(3, merchant.getName());
            statement.setString(4, merchant.getGender());
            statement.setTimestamp(5, new java.sql.Timestamp(merchant.getBirthDate().getTime()));
            statement.setLong(6, merchant.getId());
            
            statement.execute();
            statement.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean delete(Entity entity) {
        Merchant merchant = (Merchant) entity;
        
        String sql = "DELETE FROM buscador.merchant "
                + "WHERE id=?";
        
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            statement.setLong(1, merchant.getId());
            statement.execute();
            statement.close();
            
            // Deletes the User
            UserDao userDao = new UserDao();
            if(!userDao.delete(merchant.getUser())){
                return false;
            }
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public List<Entity> select() {
        List<Entity> merchants = new ArrayList();
        String sql = "SELECT * FROM buscador.merchant "
                + "INNER JOIN buscador.user ON merchant.user_id = user.id";
        
        try{
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Merchant merchant = new Merchant(new User());
                
                // Merchant Data
                merchant.setId(result.getLong("id"));
                merchant.setCreateDate(result.getDate("createDate"));
                merchant.setUpdateDate(result.getDate("updateDate"));
                merchant.setCpf(result.getString("cpf"));
                merchant.setName(result.getString("name"));
                merchant.setGender(result.getString("gender"));
                merchant.setBirthDate(result.getDate("birthDate"));
                
                // User Data
                merchant.getUser().setId(result.getLong("user_id"));
                merchant.getUser().setCreateDate(result.getDate("createDate"));
                merchant.getUser().setUpdateDate(result.getDate("updateDate"));
                merchant.getUser().setEmail(result.getString("email"));
                merchant.getUser().setPassword(result.getString("password"));
                
                merchants.add(merchant);
            }
            
            result.close();
            statement.close();
            
            return  merchants;
        }catch(SQLException e){
            throw new RuntimeException(e);   
        }
    }
    
    @Override
    public Entity find(Entity entity) {
        Merchant merchant = (Merchant) entity;
        String sql = "SELECT * FROM buscador.merchant "
                + "INNER JOIN buscador.user ON merchant.user_id = user.id "
                + "WHERE merchant.id=?";
        
        try{
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setLong(1, merchant.getId());
            
            ResultSet result = statement.executeQuery();
            
            if(result.first()){
                // Merchant Data
                merchant.setId(result.getLong("id"));
                merchant.setCreateDate(result.getDate("createDate"));
                merchant.setUpdateDate(result.getDate("updateDate"));
                merchant.setCpf(result.getString("cpf"));
                merchant.setName(result.getString("name"));
                merchant.setGender(result.getString("gender"));
                merchant.setBirthDate(result.getDate("birthDate"));
                
                // User Data
                merchant.getUser().setId(result.getLong("user_id"));
                merchant.getUser().setCreateDate(result.getDate("createDate"));
                merchant.getUser().setUpdateDate(result.getDate("updateDate"));
                merchant.getUser().setEmail(result.getString("email"));
                merchant.getUser().setPassword(result.getString("password"));
            }
            
            result.close();
            statement.close();
            
            return  merchant;
        }catch(SQLException e){
            throw new RuntimeException(e);   
        }
    }
 
    public Merchant getByCpf(String cpf){
        Merchant merchant = new Merchant(new User());
        String sql = "SELECT * FROM buscador.merchant WHERE merchant.cpf=?";
        
        try{
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, cpf);
            
            ResultSet result = statement.executeQuery();
            
            if(result.first()){
                merchant.setId(result.getLong("id"));
                merchant.setCreateDate(result.getDate("createDate"));
                merchant.setUpdateDate(result.getDate("updateDate"));
                merchant.setCpf(result.getString("cpf"));
                merchant.setName(result.getString("name"));
                merchant.setGender(result.getString("gender"));
                merchant.setBirthDate(result.getDate("birthDate"));
            }
            
            result.close();
            statement.close();
            
            return merchant;
        }catch(SQLException e){
            throw new RuntimeException(e);   
        }
    }
    
}
