package br.com.buscador.persistence;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.com.buscador.domain.Entity;
import br.com.buscador.domain.User;

public class UserDao implements IDao{

    private Connection conn;

    public UserDao() {
        conn = new ConnectionFactory().getConnection();
    }
    
    @Override
    public boolean save(Entity entity) {
        User user = (User) entity;
        String sql = "INSERT INTO buscador.user(createDate, updateDate, email, password)"
                    + "VALUES(?, ?, ?, ?)";
        
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            statement.setTimestamp(1, new java.sql.Timestamp(user.getCreateDate().getTime()));
            statement.setTimestamp(2, new java.sql.Timestamp(user.getUpdateDate().getTime()));
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.execute();
            statement.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Entity entity) {
        User user = (User) entity;
        
        String sql = "UPDATE buscador.user "
                + "SET updateDate=?, email=?, password=? "
                + "WHERE id=?";
        
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            statement.setTimestamp(1, new java.sql.Timestamp(user.getUpdateDate().getTime()));
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setLong(4, user.getId());
            
            statement.execute();
            statement.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean delete(Entity entity) {
        User user = (User) entity;
        String sql = "DELETE FROM buscador.user "
                + "WHERE id=?";
        
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            
            statement.setLong(1, user.getId());
            statement.execute();
            statement.close();
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public List<Entity> select(){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Entity find(Entity entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public User getByEmail(String email){
        User user = new User();
        String sql = "SELECT * FROM buscador.user where user.email=?";
        
        try{
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            
            if(result.first()){
                user.setId(result.getLong("id"));
                user.setCreateDate(result.getDate("createDate"));
                user.setUpdateDate(result.getDate("updateDate"));
                user.setEmail(result.getString("email"));
                user.setPassword(result.getString("password"));
            }
            
            result.close();
            statement.close();
            
            return user;
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    
}
