package br.com.buscador.controll;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.buscador.domain.Entity;
import br.com.buscador.controll.command.SaveCmd;
import br.com.buscador.controll.command.SelectCmd;
import br.com.buscador.controll.command.UpdateCmd;
import br.com.buscador.controll.command.DeleteCmd;
import br.com.buscador.controll.command.ICommand;
import br.com.buscador.controll.command.FindCmd;
import br.com.buscador.controll.viewHelper.IViewHelper;
import br.com.buscador.controll.viewHelper.MerchantVh;

@WebServlet(name = "Servlet",
        urlPatterns = {"/saveMerchant", "/deleteMerchant", "/listMerchant", "/findMerchant"})
public class Servlet extends HttpServlet {    
    private Map<String, IViewHelper> viewHelpers;
    private Map<String, ICommand> commands;
    
    public Servlet() {
        viewHelpers = new HashMap();
        viewHelpers.put("/Buscador/saveMerchant", new MerchantVh());
        viewHelpers.put("/Buscador/deleteMerchant", new MerchantVh());
        viewHelpers.put("/Buscador/listMerchant", new MerchantVh());
        viewHelpers.put("/Buscador/findMerchant", new MerchantVh());
                
        commands = new HashMap();
        commands.put("SAVE", new SaveCmd());
        commands.put("UPDATE", new UpdateCmd());
        commands.put("DELETE", new DeleteCmd());
        commands.put("SELECT", new SelectCmd());
        commands.put("FIND", new FindCmd());
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String uri = request.getRequestURI();
        
        IViewHelper viewHelper = viewHelpers.get(uri);

        Entity entity = viewHelper.getEntity(request);

        String cmd = request.getParameter("operation");
        ICommand command = commands.get(cmd);

        Result result = command.execute(entity);

        viewHelper.setView(result, request, response);
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet Genérica que trata todas as requisições da aplicação!";
    }

}
