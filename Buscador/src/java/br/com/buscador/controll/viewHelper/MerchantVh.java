package br.com.buscador.controll.viewHelper;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import br.com.buscador.controll.Result;
import br.com.buscador.domain.Entity;
import br.com.buscador.domain.Merchant;
import br.com.buscador.domain.User;

public class MerchantVh implements IViewHelper {
    
    @Override
    public Entity getEntity(HttpServletRequest request) {
        // Merchant data
        String id = request.getParameter("id");
        String cpf = request.getParameter("cpf");
        String name = request.getParameter("name");
        String gender = request.getParameter("gender");
        
        String date = request.getParameter("birthDate");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date birthDate = new Date();
        try {
            birthDate = dateFormat.parse(date);
        } catch (ParseException | NullPointerException ex) {
            Logger.getLogger(MerchantVh.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // User data
        String userId = request.getParameter("userId");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        
        switch(request.getParameter("operation")){
            case "SAVE":
                if(date == "" || date == null)
                    return new Merchant(cpf, name, gender, new User(email, password));
                else
                    return new Merchant(cpf, name, gender, birthDate, new User(email, password));
            case "UPDATE":
                if(date == "" || date == null)
                    return new Merchant(new Long(id), cpf, name, gender, new User(new Long(userId), email, password));
                else
                    return new Merchant(new Long(id), cpf, name, gender, birthDate, new User(new Long(userId), email, password));
            case "DELETE":
            case "FIND":
                return new Merchant(new Long(id), new User(new Long(userId)));
            case "SELECT":
                return new Merchant(new User());
        }
        
        return new Merchant(new User());
    }

    @Override
    public void setView(Result result, HttpServletRequest request, HttpServletResponse response) {
        try{
            request.setAttribute("result", result);
            
            RequestDispatcher dispatcher;
            
            switch(request.getParameter("operation")){
                case "SAVE":
                case "UPDATE":
                case "FIND":
                    dispatcher = request.getRequestDispatcher("/newMerchant.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "DELETE":
                    response.sendRedirect("/Buscador/listMerchant?operation=SELECT");
                    break;
                case "SELECT":
                    dispatcher = request.getRequestDispatcher("/listMerchant.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
            
        } catch(IOException | ServletException e){
            response.setContentType("text/html;charset=UTF-8");
            
            try (PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Erro</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Infelizmente, occoreu um erro!</h1>");
                out.println("</body>");
                out.println("</html>");
            } catch (IOException ex) {
                Logger.getLogger(MerchantVh.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
