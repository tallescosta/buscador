package br.com.buscador.controll.viewHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.buscador.controll.Result;

import br.com.buscador.domain.Entity;

public interface IViewHelper {
    
    public Entity getEntity(HttpServletRequest request);
    
    public void setView(Result result, HttpServletRequest request, HttpServletResponse response);
    
}
