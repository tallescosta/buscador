package br.com.buscador.controll.command;

import br.com.buscador.controll.Result;
import br.com.buscador.domain.Entity;

public class FindCmd extends AbstractCommand{

    @Override
    public Result execute(Entity entity) {
        return facade.find(entity);
    }
    
}
