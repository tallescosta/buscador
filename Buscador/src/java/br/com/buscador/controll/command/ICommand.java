package br.com.buscador.controll.command;

import br.com.buscador.controll.Result;
import br.com.buscador.domain.Entity;

public interface ICommand {
    
    public Result execute(Entity entity);
    
}
