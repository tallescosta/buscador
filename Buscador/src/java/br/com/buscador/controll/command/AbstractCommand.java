package br.com.buscador.controll.command;

import br.com.buscador.controll.facade.IFacade;
import br.com.buscador.controll.facade.Facade;

public abstract class AbstractCommand implements ICommand{
     
    protected IFacade facade;
    
    public AbstractCommand() {
        facade = new Facade();
    }
    
}
