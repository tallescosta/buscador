package br.com.buscador.controll.facade;

import br.com.buscador.controll.Result;
import br.com.buscador.domain.Entity;

public interface IFacade {
    
    public Result save(Entity entity);
    
    public Result update(Entity entity);
    
    public Result delete(Entity entity);
    
    public Result select(Entity entity);
    
    public Result find(Entity entity);
    
}
