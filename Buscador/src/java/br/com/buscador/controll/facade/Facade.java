package br.com.buscador.controll.facade;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import br.com.buscador.persistence.IDao;
import br.com.buscador.domain.Entity;
import br.com.buscador.validation.IStrategy;
import br.com.buscador.controll.Result;
import br.com.buscador.domain.Merchant;
import br.com.buscador.persistence.MerchantDao;
import br.com.buscador.validation.CreateDateMerchant;
import br.com.buscador.validation.ExistenceMerchant;
import br.com.buscador.validation.ValidateAtributesMerchant;

public class Facade implements IFacade {

    private Map<String, Map<String, List<IStrategy>>> requirements;
    private Map<String, IDao> persistence;
    
    private static final String SAVE = "SAVE";
    private static final String UPDATE = "UPDATE";
    private static final String DELETE = "DELETE";
    private static final String SELECT = "SELECT";
    private static final String FIND = "FIND";
    
    public Facade() {
        String merchantClass = Merchant.class.getName();
        
        // All Strategies
        IStrategy createDateEntity = new CreateDateMerchant();
        IStrategy validateMerchant = new ValidateAtributesMerchant();
        IStrategy existenceMerchant = new ExistenceMerchant();
                
        List<IStrategy> saveMerchant = new ArrayList();
        saveMerchant.add(validateMerchant);
        saveMerchant.add(existenceMerchant);
        saveMerchant.add(createDateEntity);
        
        List<IStrategy> updateMerchant = new ArrayList();
        updateMerchant.add(validateMerchant);
        updateMerchant.add(createDateEntity);
        
        List<IStrategy> deleteMerchant = new ArrayList();
        // Delete Strategies
        
        List<IStrategy> selectMerchant = new ArrayList();
        // Select Strategies
        
        List<IStrategy> findMerchant = new ArrayList();
        // Find Strategies
        
        Map<String, List<IStrategy>> contextMerchant = new HashMap();
        contextMerchant.put(SAVE, saveMerchant);
        contextMerchant.put(UPDATE, updateMerchant);
        contextMerchant.put(DELETE, deleteMerchant);
        contextMerchant.put(SELECT, selectMerchant);
        contextMerchant.put(FIND, findMerchant);
        
        requirements = new HashMap();
        requirements.put(merchantClass, contextMerchant);
        
        persistence = new HashMap();
        persistence.put(merchantClass, new MerchantDao());
    }
    
    public Result save(Entity entity){
        Map<String, List<IStrategy>> reqs = requirements.get(entity.getClass().getName());
        List<IStrategy> validations = reqs.get(SAVE);
        
        Result result = executeValidations(entity, validations);
        if(result.hasMsg())
            return result;
        
        IDao dao = persistence.get(entity.getClass().getName());
        boolean resultDao = dao.save(entity);
        
        if(!resultDao)
            result.addMsg("Um erro ocorreu no processo da sua operação, ele foi anotado e será resolvido em breve!");
        
        return result;
    }

    @Override
    public Result update(Entity entity) {
        Map<String, List<IStrategy>> reqs = requirements.get(entity.getClass().getName());
        List<IStrategy> validations = reqs.get(UPDATE);
        
        Result result = executeValidations(entity, validations);
        if(result.hasMsg())
            return result;
        
        IDao dao = persistence.get(entity.getClass().getName());
        boolean resultDao = dao.update(entity);
        
        if(!resultDao)
            result.addMsg("Um erro ocorreu no processo da sua operação, ele foi anotado e será resolvido em breve!");
        
        return result;
    }

    @Override
    public Result delete(Entity entity) {
        Map<String, List<IStrategy>> reqs = requirements.get(entity.getClass().getName());
        List<IStrategy> validations = reqs.get(DELETE);
                
        Result result = executeValidations(entity, validations);
        if(result.hasMsg())
            return result;
        
        IDao dao = persistence.get(entity.getClass().getName());
        boolean resultDao = dao.delete(entity);
        
        if(!resultDao)
            result.addMsg("Um erro ocorreu no processo da sua operação, ele foi anotado e será resolvido em breve!");
        
        return result;
    }

    @Override
    public Result select(Entity entity) {
        Map<String, List<IStrategy>> reqs = requirements.get(entity.getClass().getName());
        List<IStrategy> validations = reqs.get(SELECT);
        
        Result result = executeValidations(entity, validations);
        if(result.hasMsg())
            return result;
        
        IDao dao = persistence.get(entity.getClass().getName());
        result.setEntities(dao.select());
        
        return result;
    }
    
    @Override
    public Result find(Entity entity) {
        Map<String, List<IStrategy>> reqs = requirements.get(entity.getClass().getName());
        List<IStrategy> validations = reqs.get(FIND);
        
        Result result = executeValidations(entity, validations);
        if(result.hasMsg())
            return result;
        
        IDao dao = persistence.get(entity.getClass().getName());
        result.setEntity(dao.find(entity));
        
        return result;
    }
    
    public Result executeValidations(Entity entity, List<IStrategy> validations){
        Result result = new Result();
        
        for(IStrategy validation : validations){
            result.addMsg(validation.process(entity));
            
            if(result.hasMsg()){
                result.setEntity(entity);
                return result;
            }
        }
        
        return result;
    }
    
}
