<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.buscador.controll.Result"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Cadastre-se</title>
  <meta name="description" content="CRUD de Comerciantes utilizando a arquitetura MVC, e os padrões de projetos como: Command, Facade, Strategy, DAO.">
  <meta name="author" content="Talles Costa">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="css/skeleton/normalize.css" rel="stylesheet" type="text/css"/>
  <link href="css/skeleton/skeleton.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <div class="container">
        <div class="row" style="margin-top: 25%;">
            <div class="five columns">
                <h4>Cadastre-se.<br/>Queremos você conosco!</h4>
                <p>Realizando este cadastro, você se filiará a plataforma de aproximação de consumidores e comerciantes que será lançada em breve.</p>
            </div>

            <div class="offset-by-three four columns">
                <%
                    Result result = new Result();
                    result = (Result) request.getAttribute("result");
                    if(result != null){
                        if(result.hasMsg()){
                            String[] msgs = result.getMsg().split("\n");
                        for(String msg : msgs)
                            out.println("<p><i class='fa fa-times' aria-hidden='true' style='color: #FF0000;'></i> " + msg + "</p>"); 
                        }else{
                            out.println("<p><i class='fa fa-check' aria-hidden='true' style='color: #00FF00;'></i> Operação bem sucedida!</p>"); 
                        }   
                    }
                %>
                <form method="POST" action="createUser">
                    <label for="email">Usuário*</label>
                    <input id="email" name="email" class="u-full-width" type="email" placeholder="example@example.com" />

                    <label for="password">Senha*</label>
                    <input id="password" name="password" class="u-full-width" type="password" placeholder="******" />
                    
                    <br/>
                    
                    <button type="submit" name="operation" value="SAVE"><i class="fa fa-handshake-o" aria-hidden="true"></i> Criar</button>
                    <a class="button" href="newMerchant.jsp">Entrar <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    <!--<button type="submit" name="btnOperation" value="SAVE">Entrar <i class="fa fa-arrow-right" aria-hidden="true"></i></button>-->
                </form>
            </div>
        </div>
    </div>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>