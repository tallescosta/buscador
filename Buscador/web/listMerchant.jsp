<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.buscador.controll.Result"%>
<%@page import="br.com.buscador.domain.Entity"%>
<%@page import="br.com.buscador.domain.Merchant"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Listagem de Comerciantes</title>
  <meta name="description" content="CRUD de Comerciantes utilizando a arquitetura MVC, e os padrões de projetos como: Command, Facade, Strategy, DAO.">
  <meta name="author" content="Talles Costa">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="css/skeleton/normalize.css" rel="stylesheet" type="text/css"/>
  <link href="css/skeleton/skeleton.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <div class="container">
        <div class="row" style="margin-top: 10%;">
            <div class="row">
                <div class="offset-by-four one column">
                    <a class="button" href="newMerchant.jsp">Criar <i class="fa fa-check" aria-hidden="true"></i></a>
                </div>
                <div class="offset-by-one one column">
                    <a class="button" href="listMerchant?operation=SELECT">Listar <i class="fa fa-search" aria-hidden="true"></i></a>
                </div>
            </div>
            <hr>
            
            <div class="offset-by-one ten columns">  
                <h4>Listagem de Comerciantes</h4>
                
                <%
                    Result result = new Result();
                    result = (Result) request.getAttribute("result");
                    if(result != null){
                        if(result.hasMsg()){
                            String[] msgs = result.getMsg().split("\n");
                            out.println("<p>");
                        for(String msg : msgs)
                            out.println("<i class='fa fa-times' aria-hidden='true' style='color: #FF0000;'></i> " + msg + "<br/>");
                        }else{
                            out.println("<table class='u-full-width'>");
                            out.println("<thead>");
                            out.println("<tr>");
                            out.println("<td>CPF</td>");
                            out.println("<td>Nome</td>");
                            out.println("<td>Sexo</td>");
                            out.println("<td>Data Nasc.</td>");
                            out.println("<td>E-mail</td>");
                            out.println("<td>Editar</td>");
                            out.println("<td>Excluir</td>");
                            out.println("</tr>");
                            out.println("</thead>");
                            out.println("<tbody>");
                            
                            int i = 0;
                            
                            for(Entity entity : result.getEntities()){
                                Merchant merchant = (Merchant) entity;
                                                                
                                out.println("<tr>");
                                out.println("<td>" + merchant.getCpf() + "</td>");
                                out.println("<td>" + merchant.getName() + "</td>");
                                out.println("<td>" + merchant.getGender() + "</td>");
                                out.println("<td>" + merchant.getBirthDate().toString().replace("-", "/") + "</td>");
                                out.println("<td>" + merchant.getUser().getEmail() + "</td>");
                                out.println("<td>"
                                                + "<a href='findMerchant?operation=FIND&id=" + merchant.getId() + "&userId=" + merchant.getUser().getId() + "'>"
                                                    + "<i class='fa fa-pencil' aria-hidden='true'></i>"
                                                + "</a>"
                                            + "</td>");
                                
                                out.println("<td>"
                                                + "<a href='deleteMerchant?operation=DELETE&id=" + merchant.getId() + "&userId=" + merchant.getUser().getId() + "'>"
                                                    + "<i class='fa fa-trash' aria-hidden='true'></i>"
                                                + "</a>"
                                            + "</td>");
                                
                                out.println("</tr>");
                                  
                                i++;
                            }
                            
                            out.println("</tbody>");
                            out.println("</table>");
                                
                            out.println("<p>" + i + " registros encontrados.</p>"); 
                        }
                        out.println("</p>");
                    }
                %>
            </div>
        </div>
    </div>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>