<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.buscador.controll.Result"%>
<%@page import="br.com.buscador.domain.Merchant"%>
<%@page import="br.com.buscador.domain.User"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Cadastre-se</title>
  <meta name="description" content="CRUD de Comerciantes utilizando a arquitetura MVC, e os padrões de projetos como: Command, Facade, Strategy, DAO.">
  <meta name="author" content="Talles Costa">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="css/skeleton/normalize.css" rel="stylesheet" type="text/css"/>
  <link href="css/skeleton/skeleton.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <%
    String operation = "SAVE";
    Merchant merchant = new Merchant(new User());
    Result result;
    
    result = (Result) request.getAttribute("result");
    
    if(result != null){        
        if(result.hasEntities())
            merchant = (Merchant) result.getEntity(0);
    }
  %>
    <div class="container">
        <div class="row" style="margin-top: 10%;">
            <div class="row">
                <div class="offset-by-four one column">
                    <a class="button" href="newMerchant.jsp">Criar <i class="fa fa-check" aria-hidden="true"></i></a>
                </div>
                <div class="offset-by-one one column">
                    <a class="button" href="listMerchant?operation=SELECT">Listar <i class="fa fa-search" aria-hidden="true"></i></a>
                </div>
            </div>
            <hr>
            
            <form method="POST" action="saveMerchant">
                <div class="row">
                    <input name="id" class="u-full-width" type="hidden" <% if(merchant.getId() != null) out.println("value='" + merchant.getId()+ "'"); %> />
                    <input name="userId" class="u-full-width" type="hidden" <% if(merchant.getUser().getId() != null) out.println("value='" + merchant.getUser().getId() + "'"); %> />
                    
                    <div class="offset-by-one five columns">
                        <label for="cpf">CPNJ*</label>
                        <input id="cpf" name="cpf" class="u-full-width" type="text" <% if(merchant.getCpf() != null) out.println("value='" + merchant.getCpf() + "'"); %> />
                    </div>
                    <div class="offset-by-one five columns">
                        <%
                            if(result != null){
                                if(result.hasMsg()){
                                    String[] msgs = result.getMsg().split("\n");
                                    out.println("<p>");
                                    for(String msg : msgs)
                                        out.println("<i class='fa fa-times' aria-hidden='true' style='color: #FF0000;'></i> " + msg + "<br/>");
                                    out.println("</p>");
                                }else if(!result.hasEntities()){
                                    out.println("<p><i class='fa fa-check' aria-hidden='true' style='color: #00FF00;'></i> Operação bem sucedida!</p>"); 
                                }
                            }
                        %>
                    </div>
                </div>
                    
                <div class="row">
                    <div class="offset-by-one ten columns">
                        <label for="name">Nome*</label>
                        <input id="name" name="name" class="u-full-width" type="text" <% if(merchant.getName() != null ) out.println("value='" + merchant.getName() + "'"); %> />
                    </div>
                </div>
                    
                <div class="row">
                    <div class="offset-by-one five columns">
                        <label for="birthDate">Data Nasc.*</label>
                        <input id="birthDate" name="birthDate" class="u-full-width" type="text" placeholder="aaaa/mm/dd" <% if(merchant.getBirthDate() != null && merchant.getId() != null ) out.println("value='" + merchant.getBirthDate().toString().replace("-", "/") + "'"); %> />
                    </div>
                    <div class="five columns">
                        <label for="gender">Sexo*</label>
                        <input id="gender" name="gender" class="u-full-width" type="text" <% if(merchant.getGender() != null ) out.println("value='" + merchant.getGender()+ "'"); %> />
                    </div>
                </div>
                    
                <div class="row">
                    <div class="offset-by-one five columns">
                        <label for="email">Usuário*</label>
                        <input id="email" name="email" class="u-full-width" type="email" placeholder="example@example.com" />
                    </div>
                    <div class="five columns">
                        <label for="password">Senha*</label>
                        <input id="password" name="password" class="u-full-width" type="password" placeholder="******" />
                    </div>
                </div>
                    
                <div class="row">
                    <div class="offset-by-one five columns">
                        <% if(merchant.getId() != null) operation = "UPDATE"; %>
                        <button type="submit" name="operation" <% out.println("value='" + operation + "'");%> class="button-primary">Salvar <i class="fa fa-check" aria-hidden="true"></i></button>
                        <%
                            if(merchant.getId() != null)
                                out.println("<a class='button' href='listMerchant?operation=SELECT'>Cancelar <i class='fa fa-cancel' aria-hidden='true'></i></a>"); 
                        %>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>